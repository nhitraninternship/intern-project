function savePost(data) {
  var userId = firebase.auth().currentUser;
  var firebaseRef = firebase.database().ref('Posts/'+ userId.uid);
  firebaseRef.set({
    title: data.title,
    startDate: data.startDate,
    office: data.office,
    sumary: data.sumary,
    content: data.content
  });
}

function getParms() {
  var content = (((tinyMCE.get('main-content').getContent()).replace(/(&nbsp;)*/g, "")).replace(/(<p>)*/g, "")).replace(/<(\/)?p[^>]*>/g, "");
  var data = {};
  
  data.title = $('#subject').val();
  data.startDate = $('#startdate').val();
  data.office = $('#office').val(); 
  data.sumary = $('#sumary').val();
  data.content = content;

  return data;
}

$('#saveData').on('click', function(event) {
  event.preventDefault();
  var data = getParms();
   console.log(data);
  if (typeof data != "undefined") {
    savePost(data);
    return true;
  }
  return false;
});

