$(document).ready(function() {
  var arrowSignup = $(".arrow-up-register");
  var formSignup = $(".register-form");
  var status = false,
      $signUp = $("#signUp");
  $signUp.click(function(event) {
    event.preventDefault();
    if (status == false) {
      arrowSignup.fadeIn();
      formSignup.fadeIn();
      status = true;
    } else {
      arrowSignup.fadeOut();
      formSignup.fadeOut();
      status = false;
    }
  })
})

function handleSignUp() {
  var name = $('#name').value;
  var email = $('#emails').value;
  var pass = $('#pass').value;
  var confirmPass = $('#confirmPass').value;

  if(email.length<4){
    alert("Email is incorrect format");
    return;
  }
  if(pass.length<4) {
    alert("Password too short");
    return;
  }

  firebase.auth().createUserWithEmailAndPassword(email, pass).catch(function(error) {
    var errorCode = error.code;
    var errorMessage = error.message;

    if(errorCode == 'auth/weak-password') {
      alert('The password is too weak.');
    }else {
      alert(errorMessage);
    }
    console.log(error);
  });
}
