function toggleSignIn() {
  if (firebase.auth().currentUser) {
    // firebase.auth().signOut();

  } else {
    var email = document.getElementById('txtEmail').value;
    var password = document.getElementById('txtPassword').value;
    if (email.length < 4) {
      alert('Please enter an email address.');
      return;
    }
    if (password.length < 4) {
      alert('Please enter a password.');
      return;
    }
    firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {

      var errorCode = error.code;
      var errorMessage = error.message;

      if (errorCode === 'auth/wrong-password') {
        alert('Wrong password.');
      } else {
        alert(errorMessage);
      }
      console.log(error);
      document.getElementById('signIn').disabled = false;
    });
  }
  document.getElementById('signIn').disabled = true;
  firebase.auth().onAuthStateChanged(user => {
  if(user) {
    window.location = 'post-page.html'; 
  }
});
}

function toggleLogout(){
  firebase.auth().signOut();
  window.location = 'signin.html';
}

function initApp() {
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      // User is signed in.
      var displayName = user.displayName;
      var email = user.email;
      var emailVerified = user.emailVerified;
      var photoURL = user.photoURL;
      var isAnonymous = user.isAnonymous;
      var uid = user.uid;
      var providerData = user.providerData;
      // [START_EXCLUDE]
      document.getElementById('status').textContent = 'Signed In';
      document.getElementById('logout').textContent = 'Sign Out';
      document.getElementById('detailsAcount').textContent = JSON.stringify(user, null, '  ');
      if (!emailVerified) {
        document.getElementById('confirmEmail').disabled = false;
      }
    } else {
      document.getElementById('status').textContent = 'Signed out';
      document.getElementById('logout').textContent = 'Sign in';
      document.getElementById('detailsAcount').textContent = 'null';
    }
    document.getElementById('signIn').disabled = false;
  });

  document.getElementById('login').addEventListener('click', toggleSignIn, false);
  document.getElementById('logout').addEventListener('click', toggleLogout, false);
  //document.getElementById('register').addEventListener('click', handleSignUp, false);
  //document.getElementById('confirmEmail').addEventListener('click', sendEmailVerification, false);
  //document.getElementById('resetPass').addEventListener('click', sendPasswordReset, false);
}

window.onload = function() {
  initApp();
};