 This is  README file explain how to use the example project "School News" of intern.
 
 The website project include HTML, CSS Javascript, jQuery, Firebase source code files.
 # SCHOOL NEWS #

 ## Description 

The "School News" website will be used to share the information about School. Website is also a place help user share their feeling when they register account  and write blog.

### Tech

* HTML
* CSS
* Javascript
* jQuery
* Firebase
* Docpad
* Sass

### Run Project
* You must clone code from Bitbucket to your computer: `git clone git@bitbucket.org:nhitraninternship/intern-project.git`
* After then: `cd intern-project`
* Check out to Develop feature: `git checkout develop`
* You add code into package.json below `"description"` : `"engines": {
                                        "node" : "7",
                                        "npm": "4"
                                      },`
* You must set up Docpad in intern-project folder to it build jade files into html files and scss files into css files.
    * Install Docpad:
        * ` npm install -g npm`
        * ` npm install -g docpad@6.79`
    * After then you run: `docpad install`
    * Version: `v6.79.4`
* After then build structure which is suit with project.
* Continue, Install bower, plugin with command line :
    * Install bower: `npm install -g bower`
    * Continue install: `bower install`
    * Version: `1.8.0`
* You run: `docpad run` to build files in there structure.

### Contribution guidelines ###
* Open browser: localhost:8080 to launch the website of project