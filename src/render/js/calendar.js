function calendar(){
  var day = ['Sunday','Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  var month = ['January',
'February','March', 'April','May','June','July','August','September','October','November','December'];
  var d = new Date();
  text('c-day', day[d.getDay()]);
  text('c-date', d.getDate());
  text('c-month-year', month[d.getMonth()]+' '+ (1900+d.getYear()));
};


function text( id, val){
  if(val<10){
    val =  "0" + val;
  }
  document.getElementById(id).innerHTML = val;
};

window.onload = calendar;
