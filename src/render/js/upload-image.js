var uploader = $('#uploader')[0];
  var fileButton = $('#fileButton')[0];
  fileButton.addEventListener('change', function(e){
  var file = e.target.files[0];
  var storageRef = firebase.storage().ref('img/'+file.name);
  var uploadTask = storageRef.put(file);

  uploadTask.on('state_changed', function progress(snapshot) {
    var percentage = (snapshot.bytesTransferred/snapshot.totalBytes)*100;
    uploader.value = percentage;

  }, function error(err) {


  },function() {
var userId = firebase.auth().currentUser;
   var downloadURL = uploadTask.snapshot.downloadURL;
    // firebase.database().ref('Posts/'+ userId.uid).push({

    //   downloadURL : downloadURL
    // });
    firebase.database().ref('Posts/'+ userId.uid).child("downloadURL").set(downloadURL);

  });
});